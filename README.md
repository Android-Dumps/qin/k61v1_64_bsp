## full_k61v1_64_gms-user 11 RP1A.200720.011 79 release-keys
- Manufacturer: duoqin
- Platform: mt6761
- Codename: k61v1_64_bsp
- Brand: Qin
- Flavor: full_k61v1_64_gms-user
- Release Version: 11
- Id: RP1A.200720.011
- Incremental: 79
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Qin/full_k61v1_64_gms/k61v1_64_bsp:11/RP1A.200720.011/79:user/release-keys
- OTA version: 
- Branch: full_k61v1_64_gms-user-11-RP1A.200720.011-79-release-keys
- Repo: qin/k61v1_64_bsp
